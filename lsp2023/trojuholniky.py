from fractions import Fraction

pocet = int(input())
trojuholniky = []
for i in range(pocet):
    strany = input()
    trojuholnik = [int(p) for p in strany.split()]
    trojuholniky.append(trojuholnik)

index = 0
rovnake = {}
pomery = {}
for index, j in enumerate(trojuholniky):
    j.sort()
    a,b,c = j
    pomeryList = [Fraction(a, b), Fraction(a, c)]

    if a + b <= c:
        print("not a triangle")
        continue

    if tuple(j) in rovnake.keys():
        print(f"congruent to #{rovnake[tuple(j)]}")
        continue
    rovnake[tuple(j)] = index+1

    if tuple(pomeryList) in pomery.keys():
        print(f"similar to #{pomery[tuple(pomeryList)]}")
        continue
    pomery[tuple(pomeryList)] = index+1

    if a == b and b == c:
        print("acute equilateral")
    elif a**2 + b**2 > c**2:
        if a == b or b == c:
            print("acute isosceles")
        else:
            print("acute")
    elif a**2 + b**2 == c**2:
        if a == b or b == c:
            print("right isosceles")
        else:
            print("right")
    elif a**2 + b**2 < c**2:
        if a == b or b == c:
            print("obtuse isosceles")
        else:
            print("obtuse")