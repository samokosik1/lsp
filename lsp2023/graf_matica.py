vrcholy_hrany = input()
v,h = [int(p) for p in vrcholy_hrany.split()]

cesty = []
for i in range(h):
    cesta = input()
    jednotlive_cesty = [int(q) for q in cesta.split()]
    cesty.append(jednotlive_cesty)

matica = []
for i in range(v):
    p = [0]*v
    matica.append(p)

for j in cesty:
    matica[j[0]][j[1]] = 1
    matica[j[1]][j[0]] = 1

for i in matica:
    print(*i)