import itertools
x = 0
for s in itertools.product({'a', 'b', 'c', 'd'}, repeat=7):
    s = ''.join(s)
    if 'a' in s:
        print(s)
        x = x+1
print(x)