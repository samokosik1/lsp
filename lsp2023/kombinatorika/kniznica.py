def variacieBezOpak(M, k):
    if k == 1:
        vysl = []
        for x in M:
            vysl.append([x])
        return vysl
    else:
        vysl = []
        for x in M:
            for v in variacieBezOpak(M.difference({x}), k-1):
                v.append(x)
                vysl.append(v)
        return vysl

print(variacieBezOpak({1,2,3},3))
"""
from itertools import product
for a, b, c, d in product ({1, 3, 4, 9}, repeat=4):
    print(a, b, c, d, sep='')
"""