readme, because the files are kinda weird.

so there's two zip files here:
- sisyphus, the backend JS/SQLite monstrosity that handles things;
- icarus, the overly ambitious challenge-grind of a webpage.

sisyphus is the "main" part of the assignment, while icarus exists to complete challenges. i've taken this opportunity to also submit last week's challenge which (should) work okay. the distinction between last week and this week's code should be fairly obvious, as they're in different zip files.
sorry about the lack of npm test, just couldn't make it
but honestly i just hope the documentation's okay lol

anyway, i've spent the last 24 hours gazing into this codebase almost nonstop; if anything, please shout at me on whatever online platform is most convenient.

cheers!