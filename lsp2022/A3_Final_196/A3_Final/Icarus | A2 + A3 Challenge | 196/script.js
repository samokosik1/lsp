const apiRoot = "http://localhost:3000/gallery";

var authorList = document.getElementById("author-list");
const galleryDiv = document.getElementById("gallery-wrapper");
const newImageForm = document.getElementById("new-image-form");
const editImageForm = document.getElementById("edit-image-form");

const imageViewerModal = document.getElementById("image-viewer-modal");

var currentState = [];
var authorFilter = null;
var searchFilter = null;

imageViewerModal.addEventListener("show.bs.modal", function (event) {

    let button = event.relatedTarget;
    let metadataObject = button.children[1].children[1];

    createModal(getImageData(metadataObject)); // gets image data from meta tag within button; passes this as an object to createModal function.

})

function getImageData(metadataObject) {
    
    let imageData = {};

    imageData = {
        id: metadataObject.attributes["item_id"].textContent,
        author: metadataObject.attributes["author"].textContent,
        image: metadataObject.attributes["image"].textContent,
        alt: metadataObject.attributes["alt"].textContent,
        tags: metadataObject.attributes["tags"].textContent,
        description: metadataObject.attributes["description"].textContent,
    };

    return imageData;
}

function createModal(imageData) {
    
    let modalTitle = document.querySelector(".custom-modal-title");
    let modalAuthorName = document.querySelector(".modal-author-name");
    let modalImage = document.querySelector(".modal-viewer-image");
    let modalDescription = document.querySelector(".modal-description");
    let modalTags = document.querySelector(".modal-tags");


    modalTitle.innerHTML = imageData["author"];
    modalAuthorName.innerHTML = imageData["author"];
    modalImage.src = imageData["image"];
    modalImage.alt = imageData["alt"];
    modalDescription.innerHTML = imageData["description"];
    modalTags.innerHTML = "tags: " + imageData["tags"];
    
    // move this to be triggered by the edit button at some point

    let formTitle = document.querySelector(".custom-form-title");
    let formImage = document.querySelector(".form-image");
    let editFormAuthor = document.querySelector("#edit-form-author");
    let editFormDescription = document.querySelector("#edit-form-description");
    let editFormTags = document.querySelector("#edit-form-tags");
    let editFormUrl = document.querySelector("#edit-form-image");
    let editFormAlt = document.querySelector("#edit-form-alt");

    formTitle.innerHTML = imageData["author"];
    formImage.src = imageData["image"];
    formImage.alt = imageData["alt"];
    editFormAuthor.value = imageData["author"];
    editFormDescription.innerHTML = imageData["description"];
    editFormTags.value = imageData["tags"];
    editFormUrl.value = imageData["image"];
    editFormAlt.value = imageData["alt"];


    let idButtons = document.querySelectorAll(".id-buttons");
    for (let button of idButtons) {
        button.setAttribute("image_id", imageData["id"]);
    }

}

function createTile(imageData) {

    let newImage = document.createElement("div");

    newImage.classList.add("gallery-tile");
    newImage.innerHTML = getInnerHtml(imageData);

    galleryDiv.appendChild(newImage);

}

function createListEntry(author) {

    let newAuthor = document.createElement("div");
    newAuthor.classList.add("author");
    newAuthor.innerHTML = author;
    newAuthor.addEventListener("click", function() {

        if (this.classList.contains("selected")) {
            
            this.classList.remove("selected");
            authorFilter = null;




            


        } else {

            

            for (let currentAuthor of authorList.children) {
                currentAuthor.classList.remove("selected");
            }
        
            this.classList.add("selected");
        
            authorFilter = this.innerHTML;


        }

        updateGallery();
    });

    authorList.appendChild(newAuthor);

}

// ###################################################################


addEventListener("load", async function() {

    
    await fetchData();
    
    updateFullGallery();
});

newImageForm.addEventListener("submit", function (event) {

    event.preventDefault();

    formResponse = {
        author: document.getElementById("new-image-author").value,
        image: document.getElementById("new-image-url").value,
        alt: document.getElementById("new-image-alt").value,
        tags: document.getElementById("new-image-tags").value,
        description: document.getElementById("new-image-description").value,
    };

    postJSON(formResponse);

    updateFullGallery();

});

editImageForm.addEventListener("submit", function (event) {

    event.preventDefault();

    formResponse = {
        author: document.getElementById("edit-form-author").value,
        image: document.getElementById("edit-form-image").value,
        alt: document.getElementById("edit-form-alt").value,
        tags: document.getElementById("edit-form-tags").value,
        description: document.getElementById("edit-form-description").value,
    }

    let id = document.getElementById("overwrite-button").getAttribute("image_id");

    sendPUT(formResponse, id);

});

document.getElementById("wipe-button").addEventListener("click", function (event) {

    deleteAll();

});

document.getElementById("delete-button").addEventListener("click", function(event) {

    let id = this.getAttribute("image_id")

    deleteItem(id);

});

async function postJSON(jsonBody) {

    await fetch(apiRoot, {
                        method: "POST", 
                        headers: {
                            "Content-Type": "application/json",
                        },body: JSON.stringify(jsonBody)})
        .then ((response) => response.json())
        .then (jsonBody => {console.log("SUCCESS:", jsonBody);});

    await updateFullGallery();
}

async function sendPUT(jsonBody, id) {

    await fetch(apiRoot + "/" + id, {
        method: "PUT", 
        headers: {
            "Content-Type": "application/json",
        },body: JSON.stringify(jsonBody)})
.then (jsonBody => {console.log("SUCCESS:", jsonBody);});

await updateFullGallery();
}

async function deleteAll() {
    await fetch(
        apiRoot, {
        method: "DELETE",
        headers: {
            "Content-Type":"application/json",
        }, 
        body: "{}",}
    )
    await updateFullGallery();
}

async function deleteItem(id) {
    await fetch(
        apiRoot + "/" + id, {
        method: "DELETE",
        headers: {
            "Content-Type":"application/json",
        }, 
        body: "{}",}
    )

    await updateFullGallery();
}




















// ###################################################################

function isVisible(image) {

    if (authorFilter) {

        if (image["author"] != authorFilter) {return false;}

    }

    if (searchFilter) {

        return (image["author"].includes(searchFilter) || image["tags"].includes(searchFilter));

    }

    return true;

}

async function updateGallery() { // USE THIS FOR SEARCH FUNCTION

    await clearGallery();
    await fetchData();
    
    for (let i of currentState) {

        if (isVisible(i)) createTile(i);
    }
}

async function updateFullGallery() { // USE THIS FOR SEARCH FUNCTION

    await clearFullGallery();

    await fetchData();

    let authorsListed = [];
    
    for (let i of currentState) {

        if (isVisible(i)) createTile(i);

        if (!authorsListed.includes(i["author"])) {
            createListEntry(i["author"]);
            authorsListed.push(i["author"]);
        }
        
    }
}

document.getElementById("great-filter").addEventListener("input", function() {
    searchFilter = this.value;
    updateGallery();
});










async function clearFullGallery() {
    
    galleryDiv.innerHTML = "";
    authorList.innerHTML = "";

}
async function clearGallery() {
    
    galleryDiv.innerHTML = "";

}

async function fetchData () {
    currentState = await (await fetch(apiRoot)).json();
}

// the following code basically swallowed all the bad pills
// at this point the only way to make any of this look pretty again is start from scratch
// which i'll probably do next week because i hate myself

function getInnerHtml(imageData) {
return ` 
<div class="gallery-tile-inner">
<button type="button" data-bs-toggle="modal" data-bs-target="#image-viewer-modal">
    
            <div class="image-wrapper">
                <img src="${imageData["image"]}" alt="${imageData["alt"]}" content-id="${imageData["id"]}">
            </div>
            <div class="gallery-tile-info">
                <h3>${imageData["author"]}</h3>
                <div item_id="${imageData["id"]}" author="${imageData["author"]}" image="${imageData["image"]}" tags="${imageData["tags"]}" description="${imageData["description"]}" alt="${imageData["alt"]}"></div>
            </div>
    
        </button>
    </div>
    `;
}
