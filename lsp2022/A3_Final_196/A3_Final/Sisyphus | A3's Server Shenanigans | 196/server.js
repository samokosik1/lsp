const sqlite = require('sqlite3').verbose();
const express = require("express");
const cors = require("cors");
let db = my_database('./gallery.db');

var app = express();

app.use(express.json());
app.use(cors());

// ###############################################################################
// Enable the following to enable 404 errors on /gallery GET with empty database:
// Const defaults to false
// yes this is described in the documentation

const throw404OnEmptyGallery = false;

// ###############################################################################


app.get("/gallery", function(req, res) {

	db.all(`SELECT * FROM gallery`, function(err, rows) {
		
		if (err) {
			res.status(400); // for misc catches
			res.json(err);
			return;
		}
		
		if (throw404OnEmptyGallery && !rows.length) {
			res.status(404); 
			res.json({"Message":"Not Found"}); 
			return;
		} 
		/* ^^^^^^^^^^ This is optional behaviour. I have no idea whether an empty database should trigger a 404, as you could reasonably load a site with an empty database.
			* In that case, the page would need to be able to either handle a 404, or an empty array. Whatever the case, you'll need to account for it.
			* Therefore, this is a constant that's configurable on (currently) Line 16.
		// */

		res.status(200);
		res.json(rows);
		return;
		
	});
});

app.get("/gallery/:id", function(req, res) {

	const id = req.params.id.trim(); // doing this to avoid some JS type edge cases

	if (isNaN(id)) {
		res.status(400); 
		res.json({
			"Message": "Bad Request"
		});
		return;
	}

	db.all(`SELECT * FROM gallery WHERE id=?`, [id], function(err, rows) {
		
		if (err) {
			res.status(400); // for misc catches
			res.json(err);
			return;
		}

		if (!rows.length) {
			res.status(404); 
			res.json({"Message":"Not Found"}); 
			return;
		} 

		res.status(200);
		res.json(rows[0]);
		return;
		
	});
});

app.post("/gallery", function(req, res) {

	const itemToAdd = req.body;

	// check that all fields exist
	for (let currentParam of ["author", "image", "alt", "tags", "description"]) {
		if (itemToAdd[currentParam] === null || itemToAdd[currentParam] === undefined) {
			res.status(400);
			res.json({
				"Message": "Bad Request",
			});
			return;
		}
	}

	db.run(`INSERT INTO gallery (author, alt, tags, image, description)
		VALUES (?, ?, ?, ?, ?)`,
		[itemToAdd['author'], itemToAdd['alt'], itemToAdd['tags'], itemToAdd['image'], itemToAdd['description']], function(err) {

			if (err) {
				res.status(400); // for misc catches
				res.json(err);
				return;
			}
		
			res.status(201);
			res.json(itemToAdd);
			return;
		
	});
});

app.put("/gallery/:id", function(req, res) {

	const itemToUpdate = req.body;
	const id = req.params.id;

	// check that all fields exist
	for (let currentParam of ["author", "image", "alt", "tags", "description"]) {
		if (itemToUpdate[currentParam] === null || itemToUpdate[currentParam] === undefined) {
			res.status(400);
			res.json({
				"Message": "Bad Request",
			});
			return;
		}
	}

	if (isNaN(id)) {
		res.status(400); 
		res.json({
			"Message": "Bad Request", 
		});
		return;
	}

	db.run(`UPDATE gallery
	SET author=?, alt=?, tags=?, image=?,
	description=? WHERE id=?`,
	[itemToUpdate['author'], itemToUpdate['alt'], itemToUpdate['tags'], itemToUpdate['image'], itemToUpdate['description'], id], function(err) {

		if (err) {
			res.status(400); // for misc catches
			res.json(err);
			return;
		}

		if (!this.changes) {
			res.status(404);
			res.json({
				"Message":"Not Found",
			});
			return;
		}

		res.status(204).send();
		return;

	});
});

app.delete("/gallery", function(req, res) {

	db.run(`DELETE FROM gallery`, function(err) {

		if (err) {
			res.status(400); // for misc catches
			res.json(err);
			return;
		}

		res.status(204).send();
		return;

	});

});

app.delete("/gallery/:id", function(req, res) {

	const id = req.params.id;

	if (isNaN(id)) {
		res.status(400); 
		res.json({
			"Message": "Bad Request", 
		});
		return;
	}
	
	db.run(`DELETE FROM gallery WHERE id=?`, [id], function(err) {
		
		if (err) {
			res.status(400); // for misc catches
			res.json(err);
			return;
		}

		if (!this.changes) {

			res.status(404);
			res.json({
				"Message": "Not Found",
			});
			return;
		}

		res.status(204).send();
		return;
		
	});
});

// ###############################################################################

app.listen(3000);

console.log("Initialising Sisyphus:");
console.log("Grappling with eternity");
console.log("Rendering mountain range...");
console.log("Loading physics engine...");
console.log("Rolling boulders...");
console.log("Contemplating futility...");
console.log("Facing the absurd... \n");
console.log("Now listening on: http://localhost:3000/gallery/");

// ###############################################################################

function my_database(filename) {
	// Connect to db by opening filename, create filename if it does not exist:
	var db = new sqlite.Database(filename, (err) => {
  		if (err) {
			console.error(err.message);
  		}
  		console.log('Connected to database.');
	});
	db.serialize(() => {
		db.run(`
        	CREATE TABLE IF NOT EXISTS gallery
        	 (
                    id INTEGER PRIMARY KEY,
                    author CHAR(100) NOT NULL,
                    alt CHAR(100) NOT NULL,
                    tags CHAR(256) NOT NULL,
                    image char(2048) NOT NULL,
                    description CHAR(1024) NOT NULL
		 )
		`);
		db.all(`select count(*) as count from gallery`, function(err, result) {
			if (result[0].count == 0) {
				db.run(`INSERT INTO gallery (author, alt, tags, image, description) VALUES (?, ?, ?, ?, ?)`, [
        			"Tim Berners-Lee",
        			"Image of Berners-Lee",
        			"html,http,url,cern,mit",
        			"https://upload.wikimedia.org/wikipedia/commons/9/9d/Sir_Tim_Berners-Lee.jpg",
        			"The internet and the Web aren't the same thing."
    				]);
				db.run(`INSERT INTO gallery (author, alt, tags, image, description) VALUES (?, ?, ?, ?, ?)`, [
        			"Grace Hopper",
        			"Image of Grace Hopper at the UNIVAC I console",
        			"programming,linking,navy",
        			"https://upload.wikimedia.org/wikipedia/commons/3/37/Grace_Hopper_and_UNIVAC.jpg",
				"Grace was very curious as a child; this was a lifelong trait. At the age of seven, she decided to determine how an alarm clock worked and dismantled seven alarm clocks before her mother realized what she was doing (she was then limited to one clock)."
    				]);
				console.log('Inserted default entry into empty database.');
			} else {
				console.log("Database contains", result[0].count, " item(s) at startup.");
			}
		});
	});
	return db;
}
