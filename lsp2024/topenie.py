import math

body = []
while True:
    x,y = map(float, input().split())
    if x == 0 and y == 0:
        break
    body.append((x,y))

vzdialenosti = []
for i in range(len(body)):
    vzdialenosti.append(math.sqrt(body[i][0]**2 + body[i][1]**2))
    
for j in vzdialenosti:
    h = math.pi*j**2/100
    print(f"The property will be flooded in hour {int(h)+1}.")

    