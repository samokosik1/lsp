import numpy as np

def cross(bod1, bod2, bod3, bod4):
    vektor1 = (bod2[0] - bod1[0], bod2[1] - bod1[1],0)
    vektor2 = (bod4[0] - bod3[0], bod4[1] - bod3[1],0)
    return np.cross(vektor1, vektor2)[2]

# input
body = []
n = int(input())
for i in range(n):
    x,y = map(int, input().split())
    body.append((x,y))

# vypocitaj konvex hull
body.sort()
res = []    
res2 = []
for next in body:
    while len(res) > 1 and not cross(res[-1], res[-2], next, res[-1]) > 0:
        res.pop()
    res.append(next)
for next in body:
    while len(res2) > 1 and not cross(res2[-1], res2[-2], next, res2[-1]) < 0:
        res2.pop()
    res2.append(next)

body_set = set(res+res2)
print(len(body_set))
