# input     
n,m = map(int, input().split())
zoznam_susedov = [[] for _ in range(n)]
for i in range(m):
    a,b,c = map(int, input().split())
    zoznam_susedov[a-1].append((b-1,c))
    zoznam_susedov[b-1].append((a-1,c))
x,y = map(int, input().split())
x -= 1
y -= 1
    
vzdialenosti = [float("inf") for _ in range(n)]
h = [(0,x)]
while h:
    vrchol = h.pop(h.index(min(h)))    
    if vzdialenosti[vrchol[1]] != float("inf"):
        continue
    vzdialenosti[vrchol[1]] = vrchol[0]

    for sused in zoznam_susedov[vrchol[1]]:
        if vzdialenosti[sused[0]] == float("inf"):
            h.append((sused[1]+vzdialenosti[vrchol[1]], sused[0]))
if vzdialenosti[y] == float("inf"):
    print(-1)
else:
    print(vzdialenosti[y])          