def vektor(bod1, stred):
    return (stred[0] - bod1[0], stred[1] - bod1[1])

utvary = []
while True:
    n = int(input())
    if n == 0:
        break
    body = []
    for i in range(n):
        x,y = map(float, input().split())
        body.append((x,y))
    utvary.append(body)

stredy = []
for utvar in utvary:
    suradnice_x = []
    suradnice_y = []
    for bod in utvar:
        suradnice_x.append(bod[0])
        suradnice_y.append(bod[1])
    stredy.append((sum(suradnice_x)/len(suradnice_x), sum(suradnice_y)/len(suradnice_y)))


for utvar in utvary:
    index = utvary.index(utvar)
    utvar.sort()
    polovica = len(utvar) // 2
    if len(utvar) % 2 == 1 and stredy[index] in utvar:
        utvar = utvar[:polovica] + utvar[polovica+1:]    
    utvar_half = utvar[:polovica]
    nove_body = []
    for bod in utvar_half:
        v = vektor(bod, stredy[index])
        novy_bod = (stredy[index][0] + v[0], stredy[index][1] + v[1])
        nove_body.append(novy_bod)
    nove_body.sort()
    if (utvar_half + nove_body) == utvar:
        print("V.I.P. should stay at (" +str(stredy[index][0])+ "," +str(stredy[index][1])+ ").")
    else:
        print("This is a dangerous situation!")
