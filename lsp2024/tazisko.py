import math

utvary = []
while True:
    n = int(input())
    if n == 0:
        break
    body = []
    for i in range(n):
        x,y = map(int, input().split())
        body.append((x,y))
    utvary.append(body)

for utvar in utvary:
    pocet_iteracii = len(utvar) - 1
    index = 0
    sucet1 = 0
    sucet2 = 0
    sum_x = 0
    sum_y = 0
    while index < pocet_iteracii:
        utvar.append(utvar[0])
        sucet1 += utvar[index][0] * utvar[index+1][1]
        sucet2 += utvar[index][1] * utvar[index+1][0]
        utvar.pop()
        sum_x += (utvar[index][0] + utvar[index+1][0]) * (utvar[index][0]*utvar[index+1][1] - utvar[index+1][0]*utvar[index][1])
        sum_y += (utvar[index][1] + utvar[index+1][1]) * (utvar[index][0]*utvar[index+1][1] - utvar[index+1][0]*utvar[index][1])
        index += 1
    obsah = abs((sucet1-sucet2))/2
    taz_x = (1/(6*obsah)) * sum_x
    taz_y = (1/(6*obsah)) * sum_y
    print("%.2f" % round(taz_x,2), "%.2f" % round(taz_y,2))  