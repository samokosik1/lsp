import math 

def vektor(bod1, bod2):
    return (bod2[0] - bod1[0], bod2[1] - bod1[1])

utvary = []
while True:
    n = int(input())
    if n == 0:
        break
    body = []
    for i in range(n):
        x,y = map(int, input().split())
        body.append((x,y))
    utvary.append(body)

for utvar in utvary:
    utvar.append(utvar[0])
    pocet_iteracii = len(utvar) - 1
    index = 0
    obvod = 0
    while index < pocet_iteracii:
        v = vektor(utvar[index], utvar[index+1])
        obvod += math.sqrt(v[0]**2 + v[1]**2)
        index += 1
    print("%.2f" % abs(round((obvod),2)))