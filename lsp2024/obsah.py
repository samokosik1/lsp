import math

utvary = []
while True:
    n = int(input())
    if n == 0:
        break
    body = []
    for i in range(n):
        x,y = map(int, input().split())
        body.append((x,y))
    utvary.append(body)

for utvar in utvary:
    utvar.append(utvar[0])
    pocet_iteracii = len(utvar) - 1
    index = 0
    sucet1 = 0
    sucet2 = 0
    while index < pocet_iteracii:
        sucet1 += utvar[index][0] * utvar[index+1][1]
        sucet2 += utvar[index][1] * utvar[index+1][0]
        index += 1
    print("%.2f" % abs(round((sucet1-sucet2)/2,2)))